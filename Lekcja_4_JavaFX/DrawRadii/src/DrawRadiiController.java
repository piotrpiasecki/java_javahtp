import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;

public class DrawRadiiController {

   @FXML
   private Canvas canvas;

   @FXML
   void drawPicture(ActionEvent event)
   {
      GraphicsContext gc = canvas.getGraphicsContext2D();

      int j = 20;
      int diff = 300/j;
      int x2 = 0;
      int y2 = 300;

      for (int i = 1; i <= j; i++)
      {
         x2 += diff;
         y2 -= diff;
//         gc.strokeLine(0, 0, x2, y2);
//         gc.strokeLine(300, 300, x2, y2);
//         gc.strokeLine(0, 300, x2, 300 - y2);
//         gc.strokeLine(300, 0, x2, 300 - y2);
         gc.strokeLine(0, x2, x2, 300);
         gc.strokeLine(x2, 300, 300, y2);
         gc.strokeLine(300, y2, y2, 0);
         gc.strokeLine(y2, 0, 0, x2);
      }

   }

}
