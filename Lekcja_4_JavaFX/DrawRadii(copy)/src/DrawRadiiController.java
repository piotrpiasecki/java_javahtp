import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;

public class DrawRadiiController {

   @FXML
   private Canvas canvas;

   @FXML
   void drawPicture(ActionEvent event)
   {
      GraphicsContext gc = canvas.getGraphicsContext2D();
      gc.strokeLine(0,0,100,100);
   }

}
