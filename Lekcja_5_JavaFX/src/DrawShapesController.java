import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;

public class DrawShapesController
{

   @FXML
   private Canvas canvas;

   @FXML
   void drawOvalsButtonPressed(ActionEvent event)
   {
      draw("owale");
   }

   @FXML
   void drawRectanglesButtonPressed(ActionEvent event)
   {
      draw("prostokąty");
   }
public void draw(String choice)
{
   GraphicsContext gc = canvas.getGraphicsContext2D();

   gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

   int step = 10;

   for (int i = 0; i < 10; i++)
   {
      int p = 10 + i * step;
      int k = 90 + i * step;
      switch(choice)
      {
         case "prostokąty":
            gc.strokeRect(p, p, k, k);
            break;
         case "owale":
            gc.strokeOval(p, p, k, k);
            break;
      }
   }
}
}
