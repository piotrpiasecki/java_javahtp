package Time;

import java.sql.Time;

public class Time2Test
{
   public static void main(String[] args)
   {
      Time2 t1 = new Time2();
      Time2 t2 = new Time2(2);
      Time2 t3 = new Time2(21, 34);
      Time2 t4 = new Time2(14, 17, 34);
      Time2 t5 = new Time2(t4);

      displayTime("t1: wszystkie argumenty domyślne", t1);
      displayTime("t2: podano godzinę; minuty i sekundy domyślne", t2);
      displayTime("t3: podano godzinę i minuty; sekundy domyślne", t3);
      displayTime("t4: podano godzinę, minuty i sekundy", t4);
      displayTime("t5: wskazano obiekt t4 klasy Time2", t5);

      try
      {
         Time2 t6 = new Time2(99, 99, 99);
      }
      catch (IllegalArgumentException e)
      {
         System.out.printf("%nWyjątek w trakcie inicjalizacji t6: %s%n", e.getMessage());
      }
   }

   private static void displayTime(String header, Time2 t)
   {
      System.out.printf("%s%n   %s%n   %s%n", header, t.toUniversalString(), t.toString());
   }
}
