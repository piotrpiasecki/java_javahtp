package Time;

public class Time1Test
{
   public static void main(String[] args)
   {
      Time1 time = new Time1();

      displayTime("Po utworzeniu obiektu", time);
      System.out.println();

      time.setTime(13, 27, 6);
      displayTime("Po wywołaniu setTime", time);
      System.out.println();

      try
      {
         time.setTime(99, 99, 99);
      }
      catch (IllegalArgumentException e)
      {
         System.out.printf("Wyjątek: %s%n%n", e.getMessage());
      }

      displayTime("Po wywołaniu setTime z nieprawidłowymi wartościami", time);
   }

   private static void displayTime(String header, Time1 time)
   {
      System.out.printf("%s%nFormat uniwersalny: %s%nFormat 12-godzinny: %s%n",
         header, time.toUniversalString(), time.toString());
   }
}
