package Date;

import javafx.scene.Parent;

public class Employee
{
   private String firstName;
   private String lastName;
   private Date birthDate;
   private Date hireDate;

   public Employee(String firstName, String lastName, Date birthDate, Date hireDate)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.birthDate = birthDate;
      this.hireDate = hireDate;
   }

   public String toString()
   {
      return String.format("%s %s    Zatrudniony: %s   Urodzony: %s",
         firstName,  lastName,  hireDate, birthDate);
   }
}

class EmployeeTest
{
   public static void main(String[] args)
   {
      Date birth = new Date(7, 24, 1949);
      Date hire = new Date(3, 12, 1988);
      Employee employee = new Employee("Jan", "Nowak", birth, hire);

      System.out.println(employee);
   }
}
