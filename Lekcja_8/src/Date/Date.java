package Date;

public class Date
{
   private int month;
   private int day;
   private int year;

   private static final int[] daysPerMonth =
      {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

   public Date(int month, int day, int year)
   {
      if (month <= 0 || month > 12)
      {
         throw new IllegalArgumentException("Miesiąc " + month + " musi zawierać się w zakresie 1-12!");
      }

      if (day <= 0 || (day > daysPerMonth[month] && !(month == 2 && day == 29)))
      {
         throw new IllegalArgumentException("Liczba dni " + day +
            " poza zakresem dla danego miesiąca i roku!");
      }

      if (month == 2 && day == 29 && !(year % 400 == 0 || (year % 4 == 0) && (year % 100 != 0)))
      {
         throw new IllegalArgumentException("Dzień " + day + " poza zakresem dla danego miesiąca i roku!");
      }

      this.month = month;
      this.day = day;
      this.year = year;

      System.out.printf("Kontrukcja obiektu Date dla daty %s%n", this);
   }

   public String toString()
   {
      return String.format("%d/%d/%d", day, month, year);
   }

}
