package Employee;

public class Employee {
   private static int count = 0;
   private String firstName;
   private String lastName;
   private final int HWDP = 0;

   public Employee(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
      ++count;
      System.out.printf("Konstruktor Employee: %s %s; count = %d%n",
         firstName, lastName, count);
   }

   // Pobierz imię
   public String getFirstName() {
      return firstName;
   }

   // Pobierz nazwisko
   public String getLastName() {
      return lastName;
   }

   // Metoda statyczna pobierająca licznik w zmiennej statycznej
   public static int getCount() {
      return count;
   }
}

class EmployeeTest
{
   public static void main(String[] args)
   {
      Employee employee = new Employee("Jan", "Nowak");

      System.out.println(employee);
   }
}
