import java.util.Scanner;

public class Analysis
{
   public static void main(String[] Args) {
      int resultSum = 0;
      Scanner input = new Scanner(System.in);

      for (int i = 1; i <= 10; i++) {
         System.out.printf("Podaj indeks oceny studenta nr %d (zdał: 1; nie zdał: 2): ", i);
         resultSum += input.nextInt();
      }

      System.out.printf("Egzamin zdało %d studentów. %d oblało egzamin.%n", 20 - resultSum, resultSum - 10);
//      if (resultSum - 10 <= 2) { System.out.print("Premia dla wykładowcy!" };
      System.out.print(((resultSum - 10) <= 2) ? "Premia dla wykładowcy!" : "Brak premii dla wykładowcy!");
      }
}
