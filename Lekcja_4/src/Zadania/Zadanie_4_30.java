package Zadania;

import java.util.Scanner;

public class Zadanie_4_30
{
   public static void main(String[] args)
   {
      System.out.print("Podaj szerokość kwadratu (od 1 do 20): ");
      Scanner input = new Scanner(System.in);
      int a =  input.nextInt();

      if (a >= 1 && a <=20)
      {
         rysujKwadrat(a);
      }

      else
      {
         System.out.print("Podana liczba nie spełnia warunku!");
      }
   }

   public static void rysujKwadrat(int a)
   {
      for (int i = 1; i <= a; i++)
      {
         System.out.print("*");
      }

      System.out.println();

      if (a > 1)
      {
         for (int i = 1; i <= a - 2; i++)
         {
            System.out.print("*");
            for (int j = 1; j <= a - 2; j++)
            {
               System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
         }

         for (int i = 1; i <= a; i++)
         {
            System.out.print("*");
         }
      }

   }

}
