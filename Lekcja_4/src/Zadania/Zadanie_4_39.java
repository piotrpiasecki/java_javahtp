package Zadania;

import javax.jws.Oneway;
import java.util.Scanner;

public class Zadanie_4_39
{
   public static void main(String[] args)
   {
      System.out.print("Podaj liczbę 4 cyfrową do zaszyfrowania: ");
      Scanner input = new Scanner(System.in);
      int number = input.nextInt();

      int liczbaTab[] = rozbijLiczbe(number);
      wyswietl(liczbaTab);

      int[] zaszyfrowana = szyfruj(liczbaTab);
      System.out.println("\nPo zaszyfrowaniu:");
      wyswietl(zaszyfrowana);

      System.out.print("Podaj liczbę 4 cyfrową do odszyfrowania: ");
      number = input.nextInt();

      int szyfrTab[] = rozbijLiczbe(number);
      wyswietl(szyfrTab);

      int odszyfrTab[] = deszyfruj(szyfrTab);
      wyswietl(odszyfrTab);

   }
   public static int[] rozbijLiczbe(int number)
   {
      int a1, a2, a3, a4;
      a4 = number / potega(10, 3);
      number -= a4 * potega(10, 3);
      a3 = number / potega(10, 2);
      number -= a3 * potega(10, 2);
      a2 = number / potega(10, 1);
      number -= a2 * potega(10, 1);
      a1 = number;

      int[] cyfry = new int[] {a4, a3, a2, a1};
      return cyfry;
   }

   public static int potega(int a, int n)
   {
      int k = 1;
      for (int i = 1; i <= n; i++)
      {
         k *= a;
      }
      return k;
   }

   public static int[] szyfruj(int[] liczbaTab)
   {
      int[] zaszyfrowana = new int[liczbaTab.length];

      for (int i = 1; i <= liczbaTab.length; i++)
      {
         zaszyfrowana[i-1] = (liczbaTab[i-1] + 7) % 10;
      }

      int pom = 0;

      pom = zaszyfrowana[0];
      zaszyfrowana[0] = zaszyfrowana[2];
      zaszyfrowana[2] = pom;
      pom = zaszyfrowana[1];
      zaszyfrowana[1] = zaszyfrowana[3];
      zaszyfrowana[3] = pom;

      return zaszyfrowana;
   }

   public static void wyswietl(int[] tablica)
   {
      System.out.println();
      for (int i = 1; i <= tablica.length; i++)
      {
         System.out.print(tablica[i-1]);
      }
      System.out.println();
   }

   public static int[] deszyfruj(int[] liczbaTab)
   {
      int[] odszyfrowana = new int[liczbaTab.length];

      for (int i = 1; i <= liczbaTab.length; i++)
      {
         odszyfrowana[i-1] = (liczbaTab[i-1] + 3) % 10;
      }

      int pom = 0;

      pom = odszyfrowana[0];
      odszyfrowana[0] = odszyfrowana[2];
      odszyfrowana[2] = pom;
      pom = odszyfrowana[1];
      odszyfrowana[1] = odszyfrowana[3];
      odszyfrowana[3] = pom;

      return odszyfrowana;
   }
}
