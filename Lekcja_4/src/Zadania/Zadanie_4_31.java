package Zadania;
import java.util.Scanner;

public class Zadanie_4_31
{
   public static void main(String[] args)
   {
      System.out.print("Podaj liczbę 5-cyfrową: ");
      Scanner input = new Scanner(System.in);
      int number = input.nextInt();

      if (number >= 10000 && number <= 99999)
      {
         sprawdzPalindromicznosc(number);
      }
      else
         {
         while (number < 10000 || number > 99999)
         {
            System.out.println("Podałeś złą liczbę");
            number = input.nextInt();
         }
         sprawdzPalindromicznosc(number);
      }

   }

   public static void sprawdzPalindromicznosc(int number)
   {
      int a1, a2, a3, a4, a5;
      a1 = number / potega(10, 4);
      number -= a1 * potega(10, 4);
      a2 = number / potega(10, 3);
      number -= a2 * potega(10, 3);
      a3 = number / potega(10, 2);
      number -= a3 * potega(10, 2);
      a4 = number / potega(10, 1);
      number -= a4 * potega(10, 1);
      a5 = number;

      if (a1 == a5 && a2 == a4)
      {
         System.out.println("Powyższa liczba jest palindromem");
         System.out.printf("%d %d %d %d %d", a1, a2, a3, a4, a5);
      }
      else
      {
         System.out.println("Powyższa liczba nie jest palindromem");
         System.out.printf("%d %d %d %d %d", a1, a2, a3, a4, a5);
      }
   }

   public static int potega(int a, int n)
   {
      int k = 1;
      for (int i = 1; i <= n; i++)
      {
         k *= a;
      }
      return k;
   }
}
