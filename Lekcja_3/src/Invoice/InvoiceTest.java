package Invoice;

import com.sun.org.apache.xpath.internal.functions.FuncQname;

import java.util.Scanner;

public class InvoiceTest {
   public static void main(String[] Args) {
      Scanner input = new Scanner(System.in);
      System.out.print("Aby wprowadzić nowy produkt wciśnij \"Enter\"");
      boolean start = input.hasNextLine();

      if (start == true) {
         newInvoice();
      }
   }

   public static void newInvoice() {
      String name = new String();
      String descr = new String();
      int pcs;
      double price;

      Scanner input = new Scanner(System.in);
      System.out.printf("%nPodaj nazwę produktu: ");
      name = input.nextLine();
      System.out.printf("%nPodaj opis produktu: ");
      descr = input.nextLine();
      System.out.printf("%nPodaj liczbę sztuk: ");
      pcs = input.nextInt();
      System.out.printf("%nPodaj cenę produktu: ");
      price = input.nextDouble();

      Invoice invoice_1 = new Invoice(name, descr, pcs, price);

      System.out.printf("%nDane do faktury:%n");
      System.out.printf("Nazwa produktu: %s%n", invoice_1.getName());
      System.out.printf("Opis produktu: %s%n", invoice_1.getDescr());
      System.out.printf("Liczba sztuk produktu: %d szt.%n", invoice_1.getPcs());
      System.out.printf("Cena jednostkowa produktu: %.2f PLN%n", invoice_1.getPrice());
      System.out.printf("Kwota do zapłaty: %.2f PLN%n", invoice_1.getInvoiceAmount(invoice_1.getPcs(), invoice_1.getPrice()));
   }
}


