package Invoice;

/**
   Utwórz klasę o nazwie Invoice, której sklep ze sprzętem może
   używać do reprezentacji faktur za sprzedane produkty. Klasa Invoice powinna
   zawierać cztery rodzaje informacji w zmiennych instancji: numer produktu (typ
   String), opis produktu (typ String), liczba zakupionych sztuk (typ int) i cena
   za sztukę (typ double). Klasa powinna mieć konstruktor inicjalizujący wszystkie
   cztery zmienne instancji. Zapewnij dla każdej zmiennej metodę ustawiającą i metodę
   pobierającą. Dodatkowo zapewnij metodę getInvoiceAmount, która wylicza kwotę
   na fakturze (mnoży liczbę sztuk przez cenę za sztukę), a następnie zwraca kwotę
   jako wartość double. Jeśli liczba sztuk nie jest dodatnia, ustaw ją na 0. Jeśli cena
   nie jest dodatnia, ustaw ją na 0.0. Napisz aplikację o nazwie InvoiceTest testującą
   możliwości klasy Invoice.
 */

public class Invoice
{
   private String name = new String();
   private String descr = new String();
   private int pcs = 0;
   private double price = 0;

   public Invoice(String name, String descr, int pcs,double price)
   {
      this.name = name;
      this.descr = descr;
      this.pcs = pcs;
      this.price = price;
   }

   public void setName(String name) { this.name = name; }
   public void setDescr(String descr) { this.descr = descr; }
   public void setPcs(int pcs) { this.pcs = pcs; }
   public void setPrice(double price) {this.price = price; }

   public String getName() { return name; }
   public String getDescr() { return descr; }
   public int getPcs() { return pcs; }
   public double getPrice() { return price; }

   public double getInvoiceAmount(int pcs, double price)
   {
      this.pcs = pcs;
      this.price = price;

      if (pcs < 0) { pcs = 0; }
      if (price < 0) { price = 0; }

      return (pcs * price);
   }
}
