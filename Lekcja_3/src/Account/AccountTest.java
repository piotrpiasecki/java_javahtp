package Account;

import java.security.ProtectionDomain;
import java.util.Scanner;

public class AccountTest
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);

//      Account myAccount = new Account();
//      Account myAccount = new Account("HWDP JP");
      Account myAccount = new Account("Brak danych", -100);
      // Ujemny balans nie jest dopuszczony przez konstruktor i zostaje zastąpiony przez domyślne 0.0

      System.out.printf("Początkowe imię to: %s,%na stan konta to: %.2f PLN%n", myAccount.getName(), myAccount.getBalance());

      System.out.print("Podaj swoje imię i nazwisko: ");
      String theName = input.nextLine();
      myAccount.setName(theName);

      System.out.print("Podaj wielkość przelewu (w PLN): ");
      double depositAmount = input.nextDouble();
      myAccount.deposit(depositAmount);

      System.out.printf("\nNowe imię i nazwisko w obiekcie myAccount to: %s,%na stan konta to: %.2f PLN%n",
         myAccount.getName(), myAccount.getBalance());

      System.out.println("Podaj wielkość wypłaty: ");
      myAccount.withdraw(input.nextDouble());

      System.out.printf("\nImię i nazwisko w obiekcie myAccount to: %s,%na stan konta po wypłacie " +
            "to: %.2f PLN%n", myAccount.getName(), myAccount.getBalance());

      displayAccount(myAccount);
   }

   public static void displayAccount(Account accountToDisplay) { System.out.printf("Właściciel: %s    Stan: %.2f",
      accountToDisplay.getName(), accountToDisplay.getBalance()); }
}
