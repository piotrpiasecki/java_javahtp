package Account;

public class Account
{
   private String name;
   private double balance;

//   public Account(){}

//   public Account(String name)
//   {
//      this.name = name;
//   }

   public Account(String name, double balance)
   {
      this.name = name;

      if (balance >= 0.0)
      {
         this.balance = balance;
      }
//      else
//      {
//         this.balance = 0.0;
//      }
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getName()
   {
      return name;
   }

   public void deposit(double depositAmount)
   {
      if (depositAmount > 0.0)
      {
         balance = balance + depositAmount;
      }
   }

   public void withdraw(double withdrawalAmount)
   {
      if (withdrawalAmount <= balance)
      {
         balance = balance - withdrawalAmount;
      }
      if (withdrawalAmount > balance)
      {
         System.out.print("Wypłata niemożliwa - potencjalna kwota przekracza stan środków.");
      }
   }

   public double getBalance()
   {
      return balance;
   }

}
