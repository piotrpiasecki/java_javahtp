//Oddzielanie cyfr w liczbie całkowitej

package CwiczeniaPoLekcji2;

import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

public class Zadanie_2_30
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);

      System.out.print("Podaj liczbę całkowitą: ");
      int number = input.nextInt();

      int length = StringUtils.length(Integer.toString(number));

      for (int i = length; i > 0; i--)
      {
         int power = 1;
         for (int j = i; j > 1; j--)
         {
            power = power * 10;
         }
         System.out.printf("%d ", number / power);
         number = number - (number/power) * power;
      }
   }
}
