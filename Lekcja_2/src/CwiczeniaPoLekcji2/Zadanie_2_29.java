// Rzutowanie

package CwiczeniaPoLekcji2;

public class Zadanie_2_29
{
   public static void main(String [] Args)
   {
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'A', (int) 'A');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'B', (int) 'B');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'C', (int) 'C');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'a', (int) 'a');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'b', (int) 'b');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", 'c', (int) 'c');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", '0', (int) '0');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", '1', (int) '1');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", '2', (int) '2');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", '$', (int) '$');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", '*', (int) '*');
      System.out.printf("Znak: \"%c\" ma wartość liczbową: %d%n", ' ', (int) ' ');
   }
}
