package CwiczeniaPoLekcji2;

import org.apache.commons.lang3.StringUtils;
import java.util.Scanner;

public class Zadanie_2_28
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);

      double pi = 3.14159;

      System.out.print("Podaj wartość promienia okręgu: r = ");
      int radius = input.nextInt();
//      int radius = 10;

      //Wersja z rozjeżdżającymi się kolumnami

      System.out.printf("Wielkość \t Wartość przybliżona \t Wartość dokładna \n");
      System.out.printf("Średnica \t\t\t %d \t\t\t %d \n", 2 * radius, 2 * radius);
      System.out.printf("Obwód \t\t %f \t\t\t\t %f \n", 2 * pi * radius, 2 * Math.PI * radius);
      System.out.printf("Pole \t\t %f \t\t\t %f \n\n", pi * radius * radius, Math.PI * Math.pow(radius, 2));

      //Wersja z równymi kolumnami

      System.out.printf("%s%s%s%n",StringUtils.rightPad("Wielkość", 15),
         StringUtils.rightPad("Wartość przybliżona", 25), StringUtils.rightPad("Wartość dokładna", 20));

      System.out.printf("%s%s%s%n",StringUtils.rightPad("Średnica", 15),
         StringUtils.rightPad(Integer.toString(2 * radius), 25),
         StringUtils.rightPad(Integer.toString(2 * radius), 20));

      System.out.printf("%s%s%s%n",StringUtils.rightPad("Obwód", 15),
         StringUtils.rightPad(Double.toString(2 * radius * pi), 25),
         StringUtils.rightPad(Double.toString(2 * radius * Math.PI), 20));

      System.out.printf("%s%s%s%n",StringUtils.rightPad("Pole", 15),
         StringUtils.rightPad(Double.toString(radius * radius * pi), 25),
         StringUtils.rightPad(Double.toString(Math.pow(radius, 2) * Math.PI), 20));
   }
}
