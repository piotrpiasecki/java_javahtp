package CwiczeniaPoLekcji2;

import java.util.Scanner;

public class Zadanie_2_26
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);

      System.out.print("Podaj x1: ");
      int x1 = input.nextInt();

      System.out.print("Podaj x2: ");
      int x2 = input.nextInt();

      if (x1 <= x2)
      {
         if (x2 % x1 == 0) System.out.printf("Liczba x2 = %d jest wielokrtonością liczby x1 = %d.", x2 , x1);
         if (x2 % x1 != 0) System.out.printf("Liczba x2 = %d nie jest wielokrtonością liczby x1 = %d.", x2 , x1);
      }

      if (x1 > x2) System.out.printf("Liczba x2 = %d jest mniejsza niż liczba x1 = %d, " +
         "więc nie może być jej wielokrotnością!", x2, x1);
   }
}
