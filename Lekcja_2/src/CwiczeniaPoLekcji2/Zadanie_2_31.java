// Tabela z kwadratami i sześcianami liczb

package CwiczeniaPoLekcji2;

import org.apache.commons.lang3.StringUtils;
import java.util.Scanner;

public class Zadanie_2_31
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);
      System.out.print("Do której liczby liczyć? ");
      int k = input.nextInt();

      System.out.printf("%s%s%s%n", StringUtils.rightPad("Liczba",10),
         StringUtils.rightPad("Kwadrat",10),
         StringUtils.rightPad("Sześcian",10));

      for (int i = 0; i <= k; i++)
      {
         System.out.printf("%s%s%s%n", StringUtils.rightPad(Integer.toString(i),10),
            StringUtils.rightPad(Integer.toString(i * i),10),
            StringUtils.rightPad(Integer.toString(i * i * i),10));
      }
   }
}
