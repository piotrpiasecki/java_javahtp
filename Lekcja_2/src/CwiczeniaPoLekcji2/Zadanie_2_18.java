package CwiczeniaPoLekcji2;

public class Zadanie_2_18
{
   public static void main(String [] Args)
   {
      // Prostokąt

      System.out.printf("*******%n");

      int k = 6;

      for(int i = 0; i < k; i++)
      {
         System.out.printf("*     *%n");
      }

      System.out.printf("*******%n");

      // Elipsa

      System.out.printf("  ***  %n");
      System.out.printf(" *   * %n");

      int l = 5;

      for(int i = 0; i < l; i++)
      {
         System.out.printf("*     *%n");
      }

      System.out.printf(" *   * %n");
      System.out.printf("  ***  %n");

      // Dalej mi się nie chce ; p
   }
}
