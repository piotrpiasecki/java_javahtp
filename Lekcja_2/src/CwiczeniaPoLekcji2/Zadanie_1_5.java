package CwiczeniaPoLekcji2;

import java.util.Scanner;

public class Zadanie_1_5
{
   public static void main(String [] Args)
   {
      // Podpunkt a)
      // Program oblicza iloczyn trzech liczb całkowitych

      // Podpunkt b)

      Scanner input = new Scanner(System.in);

      // Podpunkt c)

      System.out.print("Podaj pierwszą liczbę całkowitą: x = ");

      // Podpunkt d)

      int x = input.nextInt();

      // Podpunkt e)

      System.out.print("Podaj drugą liczbę całkowitą: y = ");

      // Podpunkt f)

      int y = input.nextInt();

      // Podpunkt g)

      System.out.print("Podaj trzecią liczbę całkowitą: z = ");

      // Podpunkt h)

      int z = input.nextInt();

      // Podpunkt i)

      int result = x * y * z;

      // Podpunkt j)

      System.out.printf("Iloczyn liczb x, y i z wynosi: x * y * z = %d", result);


   }
}
