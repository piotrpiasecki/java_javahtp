package InstrukcjeWarunkowe;

import java.util.Scanner;

public class InstrukcjeWarunkowe
{
   public static void main(String [] Args)
   {
      Scanner input = new Scanner(System.in);

      System.out.print("Podaj pierwszą liczbę całkowitą i wciśnij \"Enter\": x1 = ");
      int number1 = input.nextInt();

      System.out.print("Podaj drugą liczbę całkowitą i wciśnij \"Enter\": x2 = ");
      int number2 = input.nextInt();

      if (number1 == number2) System.out.printf("%d == %d %n", number1, number2);
      if (number1 != number2) System.out.printf("%d != %d %n", number1, number2);
      if (number1 < number2) System.out.printf("%d < %d %n", number1, number2);
      if (number1 <= number2) System.out.printf("%d <= %d %n", number1, number2);
      if (number1 > number2) System.out.printf("%d > %d %n", number1, number2);
      if (number1 >= number2) System.out.printf("%d >= %d %n", number1, number2);
   }
}
