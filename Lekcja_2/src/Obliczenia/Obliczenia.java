package Obliczenia;

import java.util.Scanner;

public class Obliczenia
{
   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in); // tworzenie obiektu Scanner do pobierania danych z konsoli

      System.out.print("Podaj pierwszą liczbę: ");
      int liczba1 = input.nextInt();

      System.out.print("Podaj drugą liczbę: ");
      int liczba2 = input.nextInt();

      int suma = liczba1 + liczba2;

      System.out.printf("Suma liczb wynosi %d%n", suma);
   }
}
