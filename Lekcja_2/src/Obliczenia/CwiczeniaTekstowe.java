package Obliczenia;

// import java.lang.String;   <--- tego pakietu nie trzeba importować, bo jest zawsze

import java.util.Scanner;

public class CwiczeniaTekstowe
{
   public static void main(String [] Args)
   {
      Scanner wsad = new Scanner(System.in);

      System.out.print("Wpisz cokolwiek i wciśnij \"Enter\": ");

      java.lang.String napis = new java.lang.String();
      napis = wsad.nextLine();
      System.out.print("\n" + napis + "\n");
   }
}
