// Wyświetlanie tekstu
/*
Wysświetlanie
tekstu
*/
/**
 * Dokumentacja
 */
package HelloWorld;

public class HelloWorld
{
   public static void main(String [] args)
   {
      System.out.println("Hello world!");
      System.out.print("Hello world!");
      System.out.print("\n\nHello\nworld!\n");
         /*
         \n - nowy wiersz
         \t - tabulator
         \r - powrót do początku wiersza z nadpisywaniem znaków
         \\ - lewy ukośnik
         \" - cudzysłów
          */

      System.out.printf("%n%s%n%s%n", "Hello", "World!");
      System.out.printf("%n%s%n%s%n%s%n", "Hello", "World!", "HWDP");
   }
}