package Employee;

import javax.jws.Oneway;

public class BasePlusCommissionEmployee2
{

   private CommissionEmployee CE;
   private double baseSalary;

//   = new CommissionEmployee(firstName, lastName,
//      socialSecurityNumber, grossSales, commissionRate);


   public BasePlusCommissionEmployee2(String firstName, String lastName, String socialSecurityNumber,
                                      double grossSales, double commissionRate, double baseSalary)
   {
      this.CE = new CommissionEmployee(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
      this.baseSalary = baseSalary;
   }

   public String getFirstName() { return this.CE.getFirstName(); }
   public String getLastName() { return this.CE.getLastName(); }
   public String getSocialSecurityNumber() { return this.CE.getSocialSecurityNumber(); }
   public double getGrossSales() { return this.CE.getGrossSales(); }
   public double getCommissionRate() { return this.CE.getCommissionRate(); }
   public double getBaseSalary() { return this.baseSalary; }

   public void setGrossSales(double grossSales)
   {
      this.CE.setGrossSales(grossSales);
   }

   public void setCommissionRate(double commissionRate)
   {
      this.CE.setCommissionRate(commissionRate);
   }

   public void setBaseSalary(double baseSalary)
   {
      this.baseSalary = baseSalary;
   }

   @Override
   public String toString()
   {
      return String.format("%s %s%n%s %.2f%n%s %.2f%n",
         "otrzymujący wynagrodzenie zasadnicze", CE.toString(),
         "wynagrodzenie zasadnicze", baseSalary, "wynagrodzenie całkowite", earnings());
   }

   public double earnings()
   {
      return baseSalary + CE.earnings();
   }
}
