package Employee;

public class BasePlusCommissionEmployee2Test {
   public static void main(String[] args) {

      BasePlusCommissionEmployee2 employee =
         new BasePlusCommissionEmployee2
         ("Jan", "Kowalski", "222-22-2222",
            10000, .06, 2000);

      System.out.println("Information about employee from accessory methods:");
      System.out.printf("%n%s %s%n", "First name is",
         employee.getFirstName());
      System.out.printf("%s %s%n", "Second name is",
         employee.getLastName());
      System.out.printf("%s %s%n", "Insurance number is",
         employee.getSocialSecurityNumber());
      System.out.printf("%s %.2f%n", "Total sales equals",
         employee.getGrossSales());
      System.out.printf("%s %.2f%n", "Commission rate equals",
         employee.getCommissionRate());
      employee.setGrossSales(5000);
      employee.setCommissionRate(.1);
      employee.setBaseSalary(3000);
      System.out.printf("%n%s:%n%n%s%n",
         "Actualized employee's data", employee);
   }
}