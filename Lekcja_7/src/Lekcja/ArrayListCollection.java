package Lekcja;

import javax.swing.*;
import java.util.ArrayList;

public class ArrayListCollection
{
   public static void main(String[] args) {

//      ArrayList<String> items = new ArrayList<String>();
      ArrayList<String> items = new ArrayList<>();
      items.add("czerwony");
      items.add(0, "żółty");
      display(items, "Dwa kolory: ");
      display(items);

      items.add("zielony");
      items.add("żółty");
      display(items);
      items.remove("żółty");
      display(items);
      items.remove(1);
      display(items);
      System.out.printf("\"czerwony\" %s na liście", items.contains("czerwony") ? "jest" : "nie jest");
   }

   public static void display(ArrayList<String> items, String header)
   {
      System.out.print(header);

      for (String item : items)
      {
         System.out.printf("%s, ", item);
      }
      System.out.println();
   }

   public static void display(ArrayList<String> items)
   {
      for (int i = 0; i < items.size(); i++)
      {
         System.out.printf("%s, ", items.get(i));
      }
      System.out.println();
   }
}
