package Lekcja;

import java.security.SecureRandom;

public class RollDie
{
   public static void main(String[] args)
   {
      SecureRandom randomNumbers = new SecureRandom();
      int[] frequency = new int[7];
      int numberOfRolls = 60_000_000;

      for (int i = 1; i <= numberOfRolls; i++)
      {
//         frequency[randomNumbers.nextInt(6) + 1] += 1;
         ++frequency[randomNumbers.nextInt(6) + 1];
      }

      System.out.printf("%s%10s%15s%n", "Ścianka", "Częstość", "Częstotliwość");

      for (int i = 1; i < frequency.length; i++)
      {
         System.out.printf("%7d%10d%15.4f%n", i, frequency[i], (double) frequency[i] / numberOfRolls);
      }
   }
}
