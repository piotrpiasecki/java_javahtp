package Lekcja;

import java.util.Arrays;

public class ArrayManipulations
{
   public static void main(String[] args)
   {
      double[] doubleArray = {8.4, 9.3, 0.2, 7.9, 3.4};

      displayArray(doubleArray, "doubleArray przed sortowaniem");

      Arrays.sort(doubleArray);

      displayArray(doubleArray, "doubleArray po sortowaniu");

      int[] intArray = new int[10];

      Arrays.fill(intArray, 7);
      displayArray(intArray, "intArray");

      int[] intArray2 = {1, 2, 3, 4, 5, 6};
      int[] intArray2Copy = new int[intArray2.length];
      System.arraycopy(intArray2, 0, intArray2Copy, 0, intArray2.length);
      displayArray(intArray2, "intArray2");
      displayArray(intArray2Copy, "intArray2Copy");

      boolean eq1 = Arrays.equals(intArray2, intArray2Copy);
      System.out.printf("%nintArray2 %s intArray2Copy", eq1 ? "==" : "!=");
      boolean eq2 = Arrays.equals(intArray, intArray2);
      System.out.printf("%nintArray %s intArray2", eq2 ? "==" : "!=");

      int[] location = new int[2];
      location[0] = Arrays.binarySearch(intArray2, 5);
      location[1] = Arrays.binarySearch(intArray, 5);


      if (location[0] >= 0)
      {
         System.out.printf("%nZnaleziono 5 w elemencie %d z intArray2%n", location[0]);
      }
      else
      {
         System.out.printf("Nie znaleziono 5 w intArray2");
      }

      if (location[1] >= 0)
      {
         System.out.printf("%nZnaleziono 5 w elemencie %d z intArray%n", location[1]);
      }
      else
      {
         System.out.printf("Nie znaleziono 5 w intArray");
      }
   }

   public static void displayArray(double[] doubleArray, String descr)
   {
      System.out.printf("%n%s: ", descr);
      for (double val : doubleArray)
      {
         System.out.printf("%.1f  ", val);
      }
   }

   public static void displayArray(int[] intArray, String descr)
   {
      System.out.printf("%n%s: ", descr);
      for (int val : intArray)
      {
         System.out.printf("%d  ", val);
      }
   }
}
