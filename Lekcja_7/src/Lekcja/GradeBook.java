package Lekcja;

public class GradeBook
{
   private String courseName;
   private int[] grades;

   public GradeBook(String courseName, int[] grades)
   {
      this.courseName = courseName;
      this.grades = grades;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }
   public String getCourseName()
   {
      return courseName;
   }
   public void setGrades(int[] grades)
   {
      this.grades = grades;
   }
   public int[] getGrades()
   {
      return grades;
   }

   public void processGrades()
   {
      outputGrades();

      System.out.printf("%nŚrednia ocen w klasie: %.2f", getAverage());

      System.out.printf("%nNajniższa ocena: %d%nNajwyższa ocena: %d%n%n", getMinGrade(), getMaxGrade());

      outputBarChart();
   }

   public double getAverage()
   {
      double total = 0;
      double average;
      for (int grade : grades)
      {
         total += grade;
      }
      average = total / (double) grades.length;

      return average;
   }

   public int getMinGrade()
   {
      int minGrade = grades[0];
      for (int grade : grades)
      {
         if (grade < minGrade)
         {
            minGrade = grade;
         }
      }
      return minGrade;
   }

   public int getMaxGrade()
   {
      int maxGrade = grades[0];
      for (int grade : grades)
      {
         if (grade > maxGrade)
         {
            maxGrade = grade;
         }
      }
      return maxGrade;
   }

   public void outputGrades()
   {
      System.out.printf("Oceny:%n%n");

      for (int i = 0; i < grades.length; i++)
      {
         System.out.printf("Student nr %-3d: %2d%n", i + 1, grades[i]);
      }
   }

   public void outputBarChart()
   {
      System.out.println("Rozkład ocen:");

      int[] freq = new int[11];

      for (int grade : grades)
      {
         ++freq[grade / 10];
      }

      for (int i = 0; i < freq.length; i++)
      {
         if (i != 10)
         {
            System.out.printf("%02d-%02d: ", i * 10, i * 10 + 9);
         }
         else
         {
            System.out.printf("%5d: ", 100);
         }

         for (int j = 0; j < freq[i]; j++)
         {
            System.out.print("*");
         }

         System.out.println();


      }
   }
}
