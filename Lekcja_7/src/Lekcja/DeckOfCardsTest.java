package Lekcja;

public class DeckOfCardsTest
{
   public static void main(String[] args)
   {
      DeckOfCards myDeckOfCards = new DeckOfCards();

      for (int i = 0; i < myDeckOfCards.getLength(); i++)
      {
         System.out.printf("%-19s", myDeckOfCards.dealCard());
         if ((i + 1) % 4 == 0)
         {
            System.out.println();
         }
      }
      System.out.println();

      myDeckOfCards.shuffle();

      for (int i = 0; i < myDeckOfCards.getLength(); i++)
      {
         System.out.printf("%-19s", myDeckOfCards.dealCard());
         if ((i + 1) % 4 == 0)
         {
            System.out.println();
         }
      }
      System.out.println();

      myDeckOfCards.shuffle();

      for (int i = 0; i < myDeckOfCards.getLength(); i++)
      {
         System.out.printf("%-19s", myDeckOfCards.dealCard());
         if ((i + 1) % 4 == 0)
         {
            System.out.println();
         }
      }
   }
}
