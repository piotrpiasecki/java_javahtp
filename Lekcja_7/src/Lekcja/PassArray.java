package Lekcja;

public class PassArray
{
   public static void main(String[] args)
   {
      int[] array = {1, 2, 3, 4, 5};

      System.out.printf("Efekt przekazania referencji do całej tablicy:%n" +
         "Wartościami oryginalnej tablicy są:%n");
      for (int value : array)
      {
         System.out.printf("   %d", value);
      }

      modifyArray(array);

      System.out.printf("%n%nWartościami zmodyfikowanej tablicy są:%n");
      for (int value : array)
      {
         System.out.printf("   %d", value);
      }

      System.out.printf("%n%nElement array[3] przed przekazaniem do metody modifyElement: %d%n", array[3]);
      modifyElement(array[3]);

      System.out.printf("%n%nElement array[3] po przekazaniu do metody modifyElement: %d%n", array[3]);

   }
   public static void modifyArray(int[] array2)
   {
      for (int i = 0; i < array2.length; i++)
      {
         array2[i] *= 2;
      }
   }

   public static void modifyElement(int value)
   {
      value *= 2;
      System.out.printf("Wartość elementu w modifyElement: %d%n", value);
   }
}
