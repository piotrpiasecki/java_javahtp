package Lekcja;

public class GradeBook2DTest
{
   public static void main(String[] args) {

      int[][] gradesArray =
         {
            {87, 96, 70},
            {68, 87, 90},
            {94, 100, 90},
            {100, 81, 82},
            {83, 65, 85},
            {78, 87, 65},
            {85, 75, 83},
            {91, 94, 100},
            {76, 72, 84},
            {87, 93, 73}
         };

      GradeBook2D gradeBook2D = new GradeBook2D("Mathematics", gradesArray);
      System.out.printf("Witamy w dzienniku ocen przedmiotu: %s%n%n", gradeBook2D.getCourseName());
      gradeBook2D.processGrades();
   }
}
