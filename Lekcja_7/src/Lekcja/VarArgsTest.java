package Lekcja;

public class VarArgsTest
{
   public static double average(double... numbers)
   {
      double total = 0;

//      for (int i = 0; i < numbers.length; i++)
      for (double d : numbers)
      {
         total += d;
      }
   return (double) total / numbers.length;
   }
}
