package Lekcja;

public class GradeBook2D {
   private String courseName;
   private int[][] grades;

   public GradeBook2D(String courseName, int[][] grades) {
      this.courseName = courseName;
      this.grades = grades;
   }

   public void setCourseName(String courseName) {
      this.courseName = courseName;
   }

   public String getCourseName() {
      return courseName;
   }

   public void setGrades(int[][] grades) {
      this.grades = grades;
   }

   public int[][] getGrades() {
      return grades;
   }

   public void processGrades()
   {
      outputGrades();

      System.out.printf("%n%s %d%n%s %d%n%n",
         "Najniższa ocena w dzienniku to:", getMinGradeEnh(),
         "Najwyższa ocena w dzienniku to:", getMaxGradeEnh());

      outputBarChart();
   }

   public int getMinGrade() {
      int minGrade = grades[0][0];
      for (int row = 0; row < grades.length; row++) {
         for (int column = 0; column < grades[row].length; column++) {
            if (grades[row][column] < minGrade) {
               minGrade = grades[row][column];
            }
         }
      }
      return minGrade;
   }

   public int getMaxGrade() {
      int maxGrade = grades[0][0];
      for (int row = 0; row < grades.length; row++) {
         for (int column = 0; column < grades[row].length; column++) {
            if (grades[row][column] < maxGrade) {
               maxGrade = grades[row][column];
            }
         }
      }
      return maxGrade;
   }

   public double getAverage()
   {
      int total = 0;
      int counter = 0;
      for (int row = 0; row < grades.length; row++) {
         for (int column = 0; column < grades[row].length; column++) {
            total += grades[row][column];
            counter++;
         }
      }
      return (double) total / counter;
   }

   public int getMinGradeEnh()
   {
      int minGrade = grades[0][0];
      for (int[] studentGrades : grades)
      {
         for (int grade : studentGrades)
         {
            if (grade < minGrade)
            {
               minGrade = grade;
            }
         }
      }
      return minGrade;
   }

   public int getMaxGradeEnh()
   {
      int maxGrade = grades[0][0];
      for (int[] steudentGrades : grades)
      {
         for (int grade : steudentGrades)
         {
            if (grade > maxGrade)
            {
               maxGrade = grade;
            }
         }
      }
      return maxGrade;
   }

   public double getAverageEnh(int[] setOfGrades)
   {
      int total = 0;
      for (int grade : setOfGrades)
      {
         total += grade;
      }
      return (double) total / setOfGrades.length;
   }

   public void outputBarChart() {
      System.out.println("Łączny rozkład ocen:");

      int[] frequency = new int[11];

      for (int[] studentGrades : grades) {
         for (int grade : studentGrades) {
            ++frequency[grade / 10];
         }
      }

      for (int count = 0; count < frequency.length; count++) {
         if (count != 10) {
            System.out.printf("%02d-%02d: ", count * 10, count * 10 + 9);
         } else {
            System.out.printf("%5d: ", 100);
         }
         for (int i = 0; i < frequency[count]; i++) {
            System.out.print("|||");
         }

         System.out.println();
      }
   }

      public void outputGrades()
      {
         System.out.printf("Oceny:%n%n");
         System.out.print("            ");

         for (int test = 0; test < grades[0].length; test++)
         {
            System.out.printf("Test %d  ", test + 1);
         }
         System.out.println("Średnia");

         for (int student = 0; student < grades.length; student++)
         {
            System.out.printf("Uczeń %2d", student + 1);

            for (int test : grades[student])
            {
               System.out.printf("%8d", test);
            }
            double average = getAverageEnh(grades[student]);
            System.out.printf("%9.2f%n", average);
         }

   }
}
