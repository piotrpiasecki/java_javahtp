package Ćwiczenia;
import java.util.InputMismatchException;
import java.util.Scanner;

public class IntegerExercises
{
   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);

      System.out.println("Podaj dwie liczby");


      try
      {
         int a = input.nextInt();
         int b = input.nextInt();
         int c = Math.addExact(a, b);
         System.out.print(c);
      }
      catch (ArithmeticException e)
      {
         System.out.print("Doszło do przepełnienia zmiennej int!");
      }
      catch (InputMismatchException e)
      {
         System.out.print("Podano zbyt dużą liczbę!");
      }
   }
}
