package Ćwiczenia;
import java.security.SecureRandom;
import java.util.IllformedLocaleException;

public class Z_7_10
{
   SecureRandom payroll = new SecureRandom();
   int noOfWorkers = 100;
   public int[][] workersData = new int[noOfWorkers][3];
   public int[][] paymentGroups = new int[11][3];

   public void simPayrolls()
   {
      for (int workerID = 0; workerID < noOfWorkers; workerID++)
      {
         workersData[workerID][0] = workerID + 1;
         workersData[workerID][1] = 2000 + payroll.nextInt(1100);
         workersData[workerID][2] = (workersData[workerID][1] - 2000) / 100;
      }
   }

   public void paymentGroups()
   {
      for (int workerID = 0; workerID < noOfWorkers; workerID++)
      {
         paymentGroups[workersData[workerID][2]][2]++;
      }

      for (int i = 0; i < paymentGroups.length; i++)
      {
         paymentGroups[i][0] = 2000 + i * 100;
         paymentGroups[i][1] = paymentGroups[i][0] + 99;

         if (paymentGroups[i][0] < 3000)
         {
            System.out.printf("Grupa %-2d (%d-%d): %-3d", i + 1,
               paymentGroups[i][0], paymentGroups[i][1], paymentGroups[i][2]);

            for (int j = 0; j < paymentGroups[i][2]; j++)
            {
               System.out.print("[|][|]");
            }
            System.out.println();
         }
         else
         {
            System.out.printf("Grupa %-2d (  >3000  ): %-3d", i + 1, paymentGroups[i][2]);
            for (int j = 0; j < paymentGroups[i][2]; j++)
            {
               System.out.print("[|][|]");
            }
            System.out.println();
         }

      }
   }
}
