package Ćwiczenia;

import java.security.SecureRandom;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;

public class Craps
{
   private enum Status {CONTINUE, WON, LOST}

   private static final Random randomNumbers = new Random();

   private static final int SNAKE_EYES = 2;
   private static final int TREY = 3;
   private static final int SEVEN = 7;
   private static final int YO_LEVEN = 11;
   private static final int BOX_CARS = 12;

   private int myPoint;
   private Status gameStatus;
   private int sumOfDice = 0;
   private int rollsCounter = 0;

   private ArrayList<Integer> gameHistory = new ArrayList<>();

   public static void main(String[] args)
   {
      int[] results = new int[2];
      int[] temp = new int[2];
      int[][] noRollsToResult = new int[2][22];
      int noOfGames = 1_000_000_000;

      for (int i = 1; i <= noOfGames; i++)
      {
         temp = playCraps();
         ++results[temp[0]];
         if (temp[1] < 21)
         {
            ++noRollsToResult[temp[0]][temp[1]];
         }
         else
         {
            ++noRollsToResult[temp[0]][21];
         }
         noRollsToResult[temp[0]][0] += temp[1];
      }
      System.out.printf("Liczba gier: %d%n%n", (results[0] + results[1]));
      System.out.printf("Udział wygranych: %28.4f %n", (double) results[1] / Arrays.stream(results).sum() * 100);
      System.out.printf("Średnia liczba wyrzuceń do zwycięstwa: %.4f%n", (double) noRollsToResult[1][0] / results[1]);
      System.out.printf("Średnia liczba wyrzuceń do porażki: %9.4f%n", (double) noRollsToResult[0][0] / results[0]);
      System.out.printf("%23s%11s%n", "Wygrane", "Przegrane");
      System.out.printf("%16s%4d%10d%n%n","TOTAL: ", results[1], results[0]);

      for (int i = 1; i <= 20; i++)
      {
         System.out.printf(" Po %2d rzutach: %6d%10d%n", i, noRollsToResult[1][i], noRollsToResult[0][i]);
      }
      System.out.printf("Po >20 rzutach: %6d%10d%n", noRollsToResult[1][21], noRollsToResult[0][21]);
   }

   public void rollDice()
   {
      int die1 = randomNumbers.nextInt(6) + 1;
      int die2 = randomNumbers.nextInt(6) + 1;
      this.sumOfDice = die1 + die2;
      this.rollsCounter++;
      this.gameHistory.add(this.sumOfDice);
   }

   public static int[] processResults(Craps game)
   {
      int[] result = new int[2];
      if (game.gameStatus == Status.WON)
      {
         result[0] = 1;
         result[1] = game.rollsCounter;
      }
      else
      {
         if (game.gameStatus == Status.LOST)
         {
            result[0] = 0;
            result[1] = game.rollsCounter;
         }
      }
      return result;
   }

   public static int[] playCraps()
   {
      int[] gameResult = new int[2];
      Craps newGame = new Craps();
      newGame.myPoint = 0;
      newGame.rollDice();

      switch (newGame.sumOfDice)
      {
         case SEVEN:
         case YO_LEVEN:
            newGame.gameStatus = Status.WON;
            gameResult = processResults(newGame);
            break;
         case SNAKE_EYES:
         case TREY:
         case BOX_CARS:
            newGame.gameStatus = Status.LOST;
            gameResult = processResults(newGame);
            break;
         default:
            newGame.gameStatus = Status.CONTINUE;
            newGame.myPoint = newGame.sumOfDice;
            break;
      }

      while (newGame.gameStatus == Status.CONTINUE)
      {
         newGame.rollDice();

         if (newGame.sumOfDice == newGame.myPoint)
         {
            newGame.gameStatus = Status.WON;
            gameResult = processResults(newGame);
         }
         else
         {
            if (newGame.sumOfDice == SEVEN)
            {
               newGame.gameStatus = Status.LOST;
               gameResult = processResults(newGame);
            }
            else
            {
               newGame.gameStatus = Status.CONTINUE;
            }
         }
      }
      return gameResult;
   }
}
