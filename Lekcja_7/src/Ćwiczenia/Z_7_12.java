package Ćwiczenia;
import java.util.Scanner;

public class Z_7_12
{
   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);
      int number;
      int[] check = new int[4];
      int checkID = 0;
      int valueFlag;
      int repFlag;

      for (int i = 0; i < (check.length + 1); i++)
      {
         valueFlag = 0;
         repFlag = 0;

         System.out.print("Podaj liczbę z przedziału [10-100]: ");
         number = input.nextInt();

         valueFlag = checkRange(number);
         repFlag = checkRep(number, checkID, check);

         while (valueFlag + repFlag != 0)
         {
            valueFlag = 0;
            repFlag = 0;
            System.out.print("Błędna liczba! Podaj liczbę z przedziału [10-100]: ");
            number = input.nextInt();
            valueFlag = checkRange(number);
            repFlag = checkRep(number, checkID, check);
         }
         if (checkID <= 3)
         {
            check[checkID] = number;
            checkID++;
         }
      }

   }
   private static int checkRep(int number, int checkID, int[] check)
   {
      int flag = 0;
         for (int j = 0; j < checkID; j++)
         {
            if (number == check[j])
            {
               System.out.println("Liczba powtarza się!");
               flag = 1;
            }
            else
            {
               flag = 0;
            }
         }
      return flag;
   }

   private static int checkRange(int number)
   {
      if (number < 10 || number > 100)
      {
         System.out.println("Liczba poza zakresem!");
         return 1;
      }
      else
      {
         return 0;
      }
   }
}
