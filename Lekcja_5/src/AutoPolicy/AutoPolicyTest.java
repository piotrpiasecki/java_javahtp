package AutoPolicy;

public class AutoPolicyTest
{
   public static void main(String[] args)
   {
      AutoPolicy policy1 = new AutoPolicy(11111, "VW GOLF", "NJ");
      AutoPolicy policy2 = new AutoPolicy(11112, "Fiat Panda", "ME");
      policyInNoFaultState(policy1);
      policyInNoFaultState(policy2);

   }
   public static void policyInNoFaultState(AutoPolicy policy)
   {
      System.out.println("Ubezpieczenie samochodu:");
      System.out.printf("Polisa nr %d; Samochód: %s;%n Stan %s %s stanem stosującym no-fault%n%n", policy.getAccountNumber(),
      policy.getMakeAndModel(), policy.getState(), (policy.isNoFaultState() ? "jest" : "nie jest"));
   }
}
