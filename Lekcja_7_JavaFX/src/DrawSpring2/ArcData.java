package DrawSpring2;

import javafx.scene.canvas.Canvas;
import javafx.scene.shape.ArcType;

public class ArcData
{
   private double centerX;
   private double centerY;
   private final double length = 180;
   private double radiusX;
   private double radiusY;
   public double startAngle;
   public final ArcType arcType = ArcType.OPEN;

   public ArcData(Canvas canvas, double arcRadius, int counter)
   {
      this.centerX = canvas.getWidth() / 2;
      this.centerY = canvas.getHeight() / 2;
      this.radiusX = arcRadius;
      this.radiusY = arcRadius;
      this.startAngle = (counter % 2) * 180;
   }

   public double getCenterX()
   {
      return centerX;
   }

   public void setCenterX(double centerX)
   {
      this.centerX = centerX;
   }

   public double getCenterY()
   {
      return centerY;
   }

   public void setCenterY(double centerY)
   {
      this.centerY = centerY;
   }

   public double getLength()
   {
      return length;
   }

   public double getRadiusX()
   {
      return radiusX;
   }

   public void setRadiusX(double radiusX)
   {
      this.radiusX = radiusX;
   }

   public double getRadiusY()
   {
      return radiusY;
   }

   public void setRadiusY(double radiusY)
   {
      this.radiusY = radiusY;
   }

   public double getStartAngle()
   {
      return startAngle;
   }

   public void setStartAngle(double startAngle)
   {
      this.startAngle = startAngle;
   }

   public ArcType getArcType()
   {
      return arcType;
   }
}