package DrawSpring2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Arc;
import javafx.scene.transform.Rotate;

public class DrawSpringController
{

   @FXML
   private Canvas canvas;

   private Arc[] arcSet;
   private ArcData arc;
   private int noOfCurls;
   private double initialArcDiameter;
   private int buttonPressCounter = 0;

   @FXML
   void drawSpringButtonPressed(ActionEvent event)
   {
      GraphicsContext gc = canvas.getGraphicsContext2D();

      initialArcDiameter = 100;

      noOfCurls = getNoOfCurls(canvas.getHeight(), initialArcDiameter);

      Arc[] arcSet = new Arc[noOfCurls];
      gc.clearRect(0,0,canvas.getWidth(), canvas.getHeight());
      gc.beginPath();
      for (int i = 0; i < noOfCurls; i++)
      {
         arc = new ArcData(canvas, arcRadius(initialArcDiameter, i), i);
         arcSet[i] = rotate(createArc(arc), buttonPressCounter, canvas);
         addArc(arcSet[i], gc);

      }
      gc.stroke();
      buttonPressCounter += 10;
   }

   private static void addArc(Arc arc, GraphicsContext gc)
   {
      gc.arc
         (
            arc.getCenterX() , arc.getCenterY(), arc.getRadiusY(),
            arc.getRadiusX(), arc.getStartAngle(), arc.getLength()
         );
   }

   private static Arc createArc(ArcData arcData)
   {
      return new Arc
         (
            arcData.getCenterX(), arcData.getCenterY(), arcData.getRadiusX(),
            arcData.getRadiusY(), arcData.getStartAngle(), arcData.getLength()
         );
   }

   private static Arc rotate(Arc arc, double angle, Canvas canvas)
   {
      Rotate rotation = new Rotate(angle, canvas.getWidth() / 2, canvas.getHeight() / 2);
      arc.getTransforms().addAll(rotation);
      return arc;
   }

   private static double arcRadius(double initialArcDiameter, int counter)
   {
      return (counter + 1) * initialArcDiameter / 2;
   }

   private static int getNoOfCurls(double canvasHeight, double diameter)
   {
      return (int) ((canvasHeight / 2) / diameter);
   }
}
