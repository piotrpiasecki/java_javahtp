package DrawSpring2;

public interface Rotatable
{
   public abstract void rotate(Rotation rotation);
}
