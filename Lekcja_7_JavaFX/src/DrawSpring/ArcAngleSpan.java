package DrawSpring;

public class ArcAngleSpan implements Rotatable
{
   private double startAngle;
   private double finishAngle;

   @Override
   public void rotate(Rotation rotation)
   {
      this.startAngle += rotation.getAngle();
      this.finishAngle += rotation.getAngle();
   }

   public ArcAngleSpan(double startAngle, double finishAngle)
   {
      this.startAngle = startAngle;
      this.finishAngle = finishAngle;
   }

   public ArcAngleSpan(double counter)
   {
      this.startAngle = (counter % 2) * 180;
      this.finishAngle = this.startAngle + 180;
   }

   public double getStartAngle()
   {
      return startAngle;
   }

   public void setStartAngle(double startAngle)
   {
      this.startAngle = startAngle;
   }

   public double getFinishAngle()
   {
      return finishAngle;
   }

   public void setFinishAngle(double finishAngle)
   {
      this.finishAngle = finishAngle;
   }
}
