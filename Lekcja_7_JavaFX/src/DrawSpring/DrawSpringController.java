package DrawSpring;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.transform.Rotate;
import java.util.Random;

public class DrawSpringController
{

   @FXML
   private Canvas canvas;

   private ArcData arcData[];
   private int noOfCurls;
   private double initialArcDiameter;
   private boolean rotation;
   private int buttonPressCounter = 0;

   @FXML
   void drawSpringButtonPressed(ActionEvent event)
   {
//      Random colorNumber = new Random();
      GraphicsContext gc = canvas.getGraphicsContext2D();

      rotation = true;

      initialArcDiameter = 100;

//      noOfCurls = 100;      // input manually to get a spring larger than canvas' area
      noOfCurls = getNoOfCurls(canvas.getHeight(), initialArcDiameter);

      arcData = new ArcData[getNoOfArcs(noOfCurls)];

      System.out.println("no   x   y   w   h   sAng   aExt");

      buttonPressCounter = 90;

      for (int i = 0; i < noOfCurls; i++)
      {
         arcData[i] = new ArcData(canvas, buttonPressCounter, initialArcDiameter, i);
         arcData[i].strokeArc(gc);
         System.out.printf("%d   %f   %f   %f   %f   %f   %f   %n",
            i, arcData[i].getX(), arcData[i].getY(), arcData[i].getW(),
            arcData[i].getH(), arcData[i].getStartAngle(), arcData[i].getArcExtent());
      }

      buttonPressCounter += 90;
/*      gc.setStroke(Color.RED);
      gc.strokeArc(100,100,100,100,0,180,ArcType.OPEN);
      gc.setStroke(Color.BLUE);
      gc.strokeArc(0,0,100,100,0,180,ArcType.OPEN);
      gc.setStroke(Color.ORANGE);
      gc.strokeArc(200,100,100,100,0,180,ArcType.OPEN);
      gc.setStroke(Color.GREEN);
      gc.strokeArc(100,200,100,100,180,180,ArcType.OPEN);*/
   }

   private static double arcDiameter(double initialArcDiameter, int counter)
   {
      return (counter + 1) * initialArcDiameter;
   }

   private static int getNoOfCurls(double canvasHeight, double diameter)
   {
      return (int) ((canvasHeight / 2) / diameter);
   }

   private static int getNoOfArcs(int noOfCurls)
   {
      return 2 * noOfCurls;
   }
}
