package DrawSpring;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DrawSpring extends Application
{
   @Override
   public void start(Stage stage) throws Exception
   {
      Parent root = FXMLLoader.load(getClass().getResource("DrawSpring.fxml"));
      Scene scene = new Scene(root);
      stage.setTitle("Spring drawing");
      stage.setScene(scene);
      stage.show();
   }
   public static void main(String[] args)
   {
      launch(args);
   }
}
