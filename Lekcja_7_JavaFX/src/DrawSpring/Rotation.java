package DrawSpring;

public class Rotation
{
   private double xCenter;
   private double yCenter;
   private double angle;

   public Rotation(double xCenter, double yCenter, double angle)
   {
      this.xCenter = xCenter;
      this.yCenter = yCenter;
      this.angle = angle;
   }

   public double getxCenter()
   {
      return xCenter;
   }

   public void setxCenter(double xCenter)
   {
      this.xCenter = xCenter;
   }

   public double getyCenter()
   {
      return yCenter;
   }

   public void setyCenter(double yCenter)
   {
      this.yCenter = yCenter;
   }

   public double getAngle()
   {
      return angle;
   }

   public void setAngle(double angle)
   {
      this.angle = angle;
   }
}
