package DrawSpring;

import javafx.scene.canvas.Canvas;

public class ArcCoordinates implements Rotatable
{
   private double x;
   private double y;

   public ArcCoordinates(Canvas canvas, double arcDistance, int counter)
   {
      double x = canvas.getWidth() / 2;   // initial value of canvas' X-coordinate
      double y = canvas.getHeight() / 2; // initial value of canvas' Y-centre

      x = shiftX(x, arcDistance, counter);  // adding a distance depending on the number of arc given by counter
      y = shiftY(y, arcDistance, counter);

      this.x = x;
      this.y = y;
   }

   public static double shiftX(double x, double arcDistance, int counter)
   {
      return x -= Math.ceil((double) counter / 2) * arcDistance;
   }

   public static double shiftY(double y, double arcDistance, int counter)
   {
      return y -= (((double) counter / 2) + 1) * arcDistance;
   }

   @Override
   public void rotate(Rotation rotation)
   {
      double xCenter = rotation.getxCenter();
      double yCenter = rotation.getyCenter();
      double angle = (rotation.getAngle() / 360) * Math.PI;
      double xTemp = this.getX();
      double yTemp = this.getY();
      this.setX(xCenter + Math.cos(angle) * (xTemp - xCenter) - Math.sin(angle) * (yTemp - yCenter));
      this.setY(yCenter + Math.sin(angle) * (xTemp - xCenter) + Math.cos(angle) * (yTemp - yCenter));
   }

   public double getX()
   {
      return x;
   }

   public void setX(double x)
   {
      this.x = x;
   }

   public double getY()
   {
      return y;
   }

   public void setY(double y)
   {
      this.y = y;
   }

}
