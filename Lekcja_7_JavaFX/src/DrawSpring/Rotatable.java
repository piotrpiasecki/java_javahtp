package DrawSpring;

public interface Rotatable
{
   public abstract void rotate(Rotation rotation);
}
