package DrawSpring;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;

public class ArcData extends Arc
{
   public double x;
   public double y;
   public double w;
   public double h;
   public double startAngle;
   public final double arcExtent = 180.0;
   public final ArcType closure = ArcType.OPEN;

   public ArcData(Canvas canvas, int buttonPressCounter, double arcDistance, int counter)
   {
      double xCenter = canvas.getWidth() / 2;
      double yCenter = canvas.getHeight() / 2;
      double angle = buttonPressCounter;

      Rotation rotation = new Rotation(xCenter, yCenter, angle);
      ArcCoordinates arcCoordinates = new ArcCoordinates(canvas, arcDistance, counter);
      ArcAngleSpan arcAngleSpan = new ArcAngleSpan(counter);

      arcCoordinates.rotate(rotation);
      arcAngleSpan.rotate(rotation);

      this.x = arcCoordinates.getX();
      this.y = arcCoordinates.getY();
      this.w = arcDistance * (counter + 1);
      this.h = arcDistance * (counter + 1);
      this.startAngle = arcAngleSpan.getStartAngle();
//      this.arcExtent = arcAngleSpan.getFinishAngle();
   }

   public void strokeArc(GraphicsContext gc)
   {
      gc.strokeArc(this.getX(), this.getY(), this.getW(),
         this.getH(), this.getStartAngle(), this.getArcExtent(), this.getClosure());
   }

   public double getX()
   {
      return x;
   }

   public void setX(double x)
   {
      this.x = x;
   }

   public double getY()
   {
      return y;
   }

   public void setY(double y)
   {
      this.y = y;
   }

   public double getW()
   {
      return w;
   }

   public void setW(double w)
   {
      this.w = w;
   }

   public double getH()
   {
      return h;
   }

   public void setH(double h)
   {
      this.h = h;
   }

   public double getArcExtent()
   {
      return arcExtent;
   }

   public ArcType getClosure()
   {
      return closure;
   }

}