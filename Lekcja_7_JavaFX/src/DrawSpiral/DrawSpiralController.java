package DrawSpiral;

import java.util.Arrays;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class DrawSpiralController
{

   @FXML
   private Canvas canvas;

   private double vertices[][];
   private int noOfVertices;

   @FXML
   void drawSpiralButtonPressed(ActionEvent event)
   {
      Random colorNumber = new Random();

      GraphicsContext gc = canvas.getGraphicsContext2D();

      final int distance = 10;
      noOfVertices = getNoOfVertices(canvas.getHeight(), distance);
      vertices = new double[noOfVertices][2];

      for (double[] row : vertices)
      {
         Arrays.fill(row, canvas.getHeight() / 2);
      }
      vertices[0][0] = canvas.getWidth() / 2;
      vertices[0][1] = canvas.getHeight() / 2;

      for (int i = 0; i < (noOfVertices / 4); i++)
      {
         int fullDistance = (i + 1) * distance;

         vertices[1 + i * 4][0] += i * distance;
         vertices[1 + i * 4][1] += fullDistance;

         vertices[2 + i * 4][0] -= fullDistance;
         vertices[2 + i * 4][1] += fullDistance;

         vertices[3 + i * 4][0] -= fullDistance;
         vertices[3 + i * 4][1] -= fullDistance;

         vertices[4 + i * 4][0] += fullDistance;
         vertices[4 + i * 4][1] -= fullDistance;
      }

      for (int i = 0; i < noOfVertices - 1; i++)
      {
         gc.setStroke(Color.rgb(colorNumber.nextInt(256),
            colorNumber.nextInt(256), colorNumber.nextInt(256)));
         gc.setLineWidth(2);
         gc.strokeLine(vertices[i][0], vertices[i][1], vertices[i + 1][0], vertices[i + 1][1]);
      }
   }

   private static int getNoOfVertices(double canvasHeight, int distance)
   {
      return (int) ((canvasHeight / 2) / distance) * 4 + 1;
   }
}
