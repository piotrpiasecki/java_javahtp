package AbstractPlusPolymorphism;

public class CommissionEmployee extends Employee
{

   private double grossSales;
   private double commissionRate;

   public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber,
                             double grossSales, double commissionRate)
   {
      super(firstName, lastName, socialSecurityNumber);
      this.grossSales = checkSalary(grossSales);
      this.commissionRate = checkSalary(commissionRate);
   }

   public double getGrossSales()
   {
      return grossSales;
   }

   public double getCommissionRate()
   {
      return commissionRate;
   }

   public void setGrossSales(double grossSales)
   {
      this.grossSales = checkSalary(grossSales);
   }

   public void setCommissionRate(double commissionRate)
   {
      this.commissionRate = checkSalary(commissionRate);
   }

   @Override
   public double earnings()
   {
      return getCommissionRate() * getGrossSales();
   }

   @Override
   public String toString()
   {
      return String.format("%s: %s%n%s %,.2f; %s: %,.2f",
         "Commission employee", super.toString(), "Gross sales", getGrossSales(),
         "Commission rate", getCommissionRate());
   }


}
