package AbstractPlusPolymorphism;

public class BasePlusCommissionEmployee extends CommissionEmployee
{
   private double baseSalary;

   public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber,
                                     double grossSales, double commissionRate, double baseSalary)
   {
      super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

      this.baseSalary = checkSalary(baseSalary);
   }

   public void setBaseSalary(double baseSalary)
   {
      this.baseSalary = checkSalary(baseSalary);
   }

   public double getBaseSalary() { return baseSalary; }

   @Override
   public String toString()
   {
      return String.format("%s %s%n%s %,.2f",
         "Base plus commission rate employee", super.toString(), "Base salary", getBaseSalary());
   }

   @Override
   public double earnings()
   {
      return getBaseSalary() + super.earnings();
   }
}