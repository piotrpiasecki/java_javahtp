package AbstractPlusPolymorphism;

public class SalariedEmployee extends Employee
{
   private double weeklySalary;

   public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber, double weeklySalary)
   {
      super(firstName, lastName, socialSecurityNumber);
      this.weeklySalary = checkSalary(weeklySalary);
   }

   public double getWeeklySalary()
   {
      return weeklySalary;
   }

   public void setWeeklySalary(double weeklySalary)
   {
      this.weeklySalary = checkSalary(weeklySalary);
   }

   @Override
   public double earnings()
   {
      return getWeeklySalary();
   }

   @Override
   public String toString()
   {
      return String.format("Salaried employee: %s%n%s: %.2f USD",
         super.toString(), "Weekly salary", getWeeklySalary());
   }
}
