package AbstractPlusPolymorphism;

public class PayrollSystemTest
{
   public static void main(String[] args)
   {
      SalariedEmployee salariedEmployee = new SalariedEmployee("Jan", "Kowalski",
         "111-11-1111", 800.00);
      HourlyEmployee hourlyEmployee = new HourlyEmployee("Anna", "Nowak",
         "222-22-2222", 16.75, 40);
      CommissionEmployee commissionEmployee = new CommissionEmployee("Zofia", "Lewandowska",
         "333-33-3333", 10_000, 0.06);
      BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Robert",
         "Czekaj", "444-44-4444", 5_000, 0.04, 300);

      System.out.println("Pracownicy przetwarzani pojedynczo:");

      System.out.printf(
         "%n%s%n%s: %,.2f USD%n%n", salariedEmployee, "Earned", salariedEmployee.earnings());
      System.out.printf(
         "%s%n%s: %,.2f USD%n%n", hourlyEmployee, "Earned", hourlyEmployee.earnings());
      System.out.printf(
         "%s%n%s: %,.2f USD%n%n", commissionEmployee, "Earned", commissionEmployee.earnings());
      System.out.printf(
         "%s%n%s: %,.2f USD%n%n", basePlusCommissionEmployee, "Earned", basePlusCommissionEmployee.earnings());

      Employee[] employees = new Employee[4];

      employees[0] = salariedEmployee;
      employees[1] = hourlyEmployee;
      employees[2] = commissionEmployee;
      employees[3] = basePlusCommissionEmployee;

      System.out.printf("Pracownicy przetwarzani polimorficznie:%n%n");

      for (Employee currentEmployee : employees)
      {
         System.out.println(currentEmployee);

         if (currentEmployee instanceof BasePlusCommissionEmployee)
         {
            BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;
            employee.setBaseSalary(1.1 * employee.getBaseSalary());
//            ((BasePlusCommissionEmployee)currentEmployee).setBaseSalary(
//               1.1 * ((BasePlusCommissionEmployee)currentEmployee).getBaseSalary());
            System.out.printf("New base salary after 10%% bonus equals: %,.2f USD%n", employee.getBaseSalary());
         }
         System.out.printf("Earned %,.2f USD%n%n", currentEmployee.earnings());
      }

      for (int j = 0; j < employees.length; j++)
      {
         System.out.printf("Pracownik %d to klasa %s%n", j, employees[j].getClass().getName());
      }
   }
}
