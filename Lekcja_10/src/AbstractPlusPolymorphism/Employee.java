package AbstractPlusPolymorphism;

public abstract class Employee
{
   private final String firstName;
   private final String lastName;
   private final String socialSecurityNumber;

   public Employee(String firstName, String lastName, String socialSecurityNumber)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.socialSecurityNumber = socialSecurityNumber;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public String getSocialSecurityNumber()
   {
      return socialSecurityNumber;
   }

   @Override
   public String toString()
   {
      return String.format("%s %s%nSocial Security Number: %s",
         getFirstName(),  getLastName(),  getSocialSecurityNumber());
   }

   public abstract double earnings();

   protected static double checkSalary(double salary)
   {
      if (salary < 0.0)
      {
         throw new IllegalArgumentException("Salary must be larger than zero!");
      }
      return salary;
   }
}
