package Polymorphism;

public class PolymorphismTest {

   public static void main(String[] args) {
      CommissionEmployee commissionEmployee = new CommissionEmployee("Jan", "Kowalski",
         "222-22-2222", 10_000, 0.06);

      BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Anna",
         "Nowak", "333-33-3333", 5000, 0.04, 300);

      System.out.printf("%s %s:%n%n%s%n%n", "Wywołanie metody toString z Polymorphism.CommissionEmployee za pomocą zmiennej",
         "do obiektu klasy nadrzędnej", commissionEmployee.toString());

      System.out.printf("%s %s:%n%n%s%n%n", "Wywołanie metody toString z Polymorphism.BasePlusCommissionEmployee za pomocą zmiennej",
         "do obiektu klasy podklasy", basePlusCommissionEmployee.toString());

      CommissionEmployee commissionEmployee2 = basePlusCommissionEmployee;

      System.out.printf("%s %s:%n%n%s%n%n", "Wywołanie metody toString z Polymorphism.BasePlusCommissionEmployee za pomocą zmiennej",
         "do obiektu nadrzędnego", commissionEmployee2.toString());

      CommissionEmployee commissionEmployee1 = new BasePlusCommissionEmployee("Anna", "Nowak",
         "333-33-3333", 5000, 0.04, 300);

   }
}
