package Polymorphism;

public class CommissionEmployee extends Object
{
   private final String firstName;
   private final String lastName;
   private final String socialSecurityNumber;
   private double grossSales;
   private double commissionRate;

   public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber,
                             double grossSales, double commissionRate)
   {
      if (grossSales < 0.0)
      {
         throw new IllegalArgumentException("Łączna sprzedaż musi być nieujemna!");
      }
      if (commissionRate < 0.0)
      {
         throw new IllegalArgumentException("Procent prowizji musi być nieujemny!");
      }

      this.firstName = firstName;
      this.lastName = lastName;
      this.socialSecurityNumber = socialSecurityNumber;
      this.grossSales = grossSales;
      this.commissionRate = commissionRate;
   }

   public String getFirstName() { return firstName; }
   public String getLastName() { return lastName; }
   public String getSocialSecurityNumber() { return socialSecurityNumber; }
   public double getGrossSales() { return grossSales; }
   public double getCommissionRate() { return commissionRate; }

   public void setGrossSales(double grossSales)
   {
      if (grossSales < 0.0)
      {
         throw new IllegalArgumentException("Łączna sprzedaż musi być nieujemna!");
      }
      this.grossSales = grossSales;
   }

   public void setCommissionRate(double commissionRate)
   {
      if (commissionRate < 0.0)
      {
         throw new IllegalArgumentException("Procent prowizji musi być nieujemny!");
      }
      this.commissionRate = commissionRate;
   }

   public double earnings() { return commissionRate * grossSales; }

   @Override
   public String toString()
   {
      return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f",
         "Pracownik prowizyjny", firstName, lastName,
         "Numer ubezpieczenia", socialSecurityNumber,
         "Łączna sprzedaż", grossSales,
         "Procent prowizji", commissionRate);
   }



}
