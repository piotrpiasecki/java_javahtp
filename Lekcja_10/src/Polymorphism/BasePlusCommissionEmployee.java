package Polymorphism;

public class BasePlusCommissionEmployee extends CommissionEmployee
{
   private double baseSalary;

   public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber,
                                     double grossSales, double commissionRate, double baseSalary)
   {
      super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

      if (baseSalary < 0.0)
      {
         throw new IllegalArgumentException("Salary must be a non-negative value!");
      }

      this.baseSalary = baseSalary;
   }

   public void setBaseSalary(double baseSalary)
   {
      if (baseSalary < 0.0)
      {
         throw new IllegalArgumentException("Salary must be a non-negative value!");
      }

      this.baseSalary = baseSalary;
   }

   public double getBaseSalary() { return baseSalary; }

   @Override
   public String toString()
   {
      return String.format("%s %s%n%s %.2f",
         "Otrzymujący wynagrodzenie zasadnicze", super.toString(), "Wynagrodzenie zasadnicze", baseSalary);
   }

   @Override
   public double earnings()
   {
      return baseSalary + super.earnings();
   }
}

//HWDP