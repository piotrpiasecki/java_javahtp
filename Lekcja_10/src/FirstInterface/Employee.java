package FirstInterface;

public abstract class Employee implements Payable
{
   private final String firstName;
   private final String lastName;
   private final String socialSecurityNumber;

   public Employee(String firstName, String lastName, String socialSecurityNumber)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.socialSecurityNumber = socialSecurityNumber;
   }


   public String getFirstName()
   {
      return firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public String getSocialSecurityNumber()
   {
      return socialSecurityNumber;
   }

   public abstract double earnings();

   @Override
   public String toString()
   {
      return String.format("%s %s%nnumer ubezpieczenia: %s",
         getFirstName(), getLastName(), getSocialSecurityNumber());
   }

   @Override
   public double getPaymentAmount()
   {
      return earnings();
   }

   protected static double checkSalary(double salary)
   {
      if (salary < 0.0)
      {
         throw new IllegalArgumentException("Salary must be larger than zero!");
      }
      return salary;
   }
}
