package FirstInterface;

public class Invoice implements Payable
{
   private final String partNumber;
   private final String partDescription;
   private final int quantity;
   private final double pricePerItem;

   public Invoice(String partNumber, String partDescription, int quantity, double pricePerItem)
   {
      this.partNumber = partNumber;
      this.partDescription = partDescription;
      this.quantity = checkQuantity(quantity);
      this.pricePerItem = checkPrice(pricePerItem);
   }

   public String getPartNumber()
   {
      return partNumber;
   }

   public String getPartDescription()
   {
      return partDescription;
   }

   public int getQuantity()
   {
      return quantity;
   }

   public double getPricePerItem()
   {
      return pricePerItem;
   }

   private static double checkPrice(double price)
   {
      if (price < 0.0)
      {
         throw new IllegalArgumentException("Price has to be grater than \"0\"!");
      }
      return price;
   }

   private static int checkQuantity(int quantity)
   {
      if (quantity < 0.0)
      {
         throw new IllegalArgumentException("Quantity has to be grater than \"0\"!");
      }
      return quantity;
   }

   @Override
   public String toString()
   {
      return String.format("%s: %n%s: %s (%s) %n%s: %d %n%s: %,.2f zł",
         "faktura", "numer części", getPartNumber(), getPartDescription(),
         "liczba sztuk", getQuantity(), "cena za sztukę", getPricePerItem());
   }

   @Override
   public double getPaymentAmount()
   {
      return getQuantity() * getPricePerItem();
   }
}