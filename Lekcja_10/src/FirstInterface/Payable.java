package FirstInterface;

public interface Payable
{
   public abstract double getPaymentAmount();
}
