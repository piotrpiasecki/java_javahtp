package FirstInterface;

public class HourlyEmployee extends Employee
{
   private double wage;
   private double hours;

   public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double wage, double hours)
   {
      super(firstName, lastName, socialSecurityNumber);
      this.wage = checkSalary(wage);
      this.hours = checkHours(hours);
   }

   public double getWage()
   {
      return wage;
   }

   public void setWage(double wage)
   {
      this.wage = checkSalary(wage);
   }

   public double getHours()
   {
      return hours;
   }

   public void setHours(double hours)
   {
      this.hours = checkHours(hours);
   }

   private static double checkHours(double hours)
   {
      if (hours < 0.0 || hours > 168)
      {
         throw new IllegalArgumentException("Working hours must be in [0-168]!");
      }
      return hours;
   }

   @Override
   public double earnings()
   {
      if (getHours() <= 40)
      {
         return getWage() * getHours();
      }
      else
      {
         return getWage() * (40 + (getHours() - 40) * 1.5) ;
      }
   }

   @Override
   public String toString()
   {
      return String.format("Hourly employee: %s%n%s: %.2f USD; %s %.2f",
         super.toString(),  "Wage", getWage(),  "Hours", getHours());
   }
}
