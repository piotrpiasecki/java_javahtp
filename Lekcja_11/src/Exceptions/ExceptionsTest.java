package Exceptions;

public class ExceptionsTest
{
   public static void main(String[] args)
   {
      try
      {
         method1();
      }
      catch (ExceptionA exceptionA)
      {
         System.out.println("ExceptionB service from main method.");
      }

      try
      {
         method2();
      }
      catch (ExceptionA exceptionA)
      {
         System.out.println("ExceptionC service from main method.");
      }
   }

   private static void method1() throws ExceptionB
   {
      throw new ExceptionB("ExceptionB thrown by method1.");
   }

   private static void method2() throws ExceptionC
   {
      throw new ExceptionC("ExceptionC thrown by method2.");
   }
}
