package Exceptions_2;

import java.io.IOException;

public class Exceptions_2Test
{
   public static void main(String[] args) throws Exception
   {
      try
      {
         throwExceptionA();
      }
      catch (Exception e)
      {
         System.out.println("ExceptionA service in main method");
         System.err.printf("%s%n", e.getMessage());
      }

      try
      {
         throwExceptionB();
      }
      catch (Exception e)
      {
         System.out.println("ExceptionB service in main method");
         System.err.printf("%s%n", e.getMessage());
      }

      try
      {
         throwNullPointerException();
      }
      catch (Exception e)
      {
         System.out.println("throwNullPointerException service in main method");
         System.err.printf("%s%n", e.getMessage());
      }

      try
      {
         throw new IOException("IOException thrown in main method.");
      }
      catch (Exception e)
      {
         System.out.println("IOException service in main method");
         System.err.printf("%s%n", e.getMessage());
      }
   }

   public static void throwExceptionA() throws ExceptionA
   {
      throw new ExceptionA("ExceptionA thrown by throwExceptionA method.");
   }

   public static void throwExceptionB() throws ExceptionB
   {
      throw new ExceptionB("ExceptionB thrown by throwExceptionB method.");
   }

   public static void throwNullPointerException()
   {
      throw new NullPointerException("NullPointerException thrown by throwNullPointerException method.");
   }
}
