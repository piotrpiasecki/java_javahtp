import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionHandling_2
{
   public static int quotient(int numerator, int denominator) throws ArithmeticException
   {
      return numerator / denominator;
   }

   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);
      boolean continueLoop = true;

      do
      {
         try
         {
            System.out.print("Input numerator (integer):");
            int numerator = input.nextInt();
            System.out.print("Input denominator (integer):");
            int denominator = input.nextInt();

            int result = quotient(numerator, denominator);
            System.out.printf("%nResult: %d / %d = %d%n", numerator, denominator, result);
            continueLoop = false;
         } catch (InputMismatchException | ArithmeticException badNumberException)
         {
            System.err.printf("%nException: %s%n", badNumberException);
            input.nextLine();
            System.out.printf("Input must consist of two integer numbers and denominator can't equal 0. Try again: %n%n");
         }
      }
      while (continueLoop == true);
   }
}
