import javafx.scene.Parent;

import java.util.IllegalFormatException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionHandling
{
   public static int quotient(int numerator, int denominator) throws ArithmeticException
   {
         return numerator / denominator;
   }

   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);
      boolean continueLoop = true;

      do
      {
         try
         {
            System.out.print("Input numerator (integer):");
            int numerator = input.nextInt();
            System.out.print("Input denominator (integer):");
            int denominator = input.nextInt();

            int result = quotient(numerator, denominator);
            System.out.printf("%nResult: %d / %d = %d%n", numerator, denominator, result);
            continueLoop = false;
         }
         catch (InputMismatchException inputMismatchException)
         {
            System.err.printf("%nException: %s%n", inputMismatchException);
            input.nextLine();
            System.out.printf("Input must consist of two integer numbers. Try again: %n%n");
         }
         catch (ArithmeticException arithmeticException)
         {
            System.err.printf("%nException: %s%n", arithmeticException);
            System.out.printf("Denumerator can't be equal to 0! Try again!%n%n");
         }
      }
      while (continueLoop == true);
   }
}
