public class UsingExceptions
{
   public static void main(String[] args)
   {
      try
      {
         throwException();
      }
      catch (Exception exception)
      {
         System.err.println("Exception service in main method");
      }
      doesNotThrowException();
   }

   public static void throwException() throws Exception
   {
      try
      {
         System.out.println("throwException method.");
         throw new Exception();
      }
      catch (Exception exception)
      {
         System.err.println("Exception service in throwException method");
         throw exception;
      }
      finally
      {
         System.err.println("Finally executed in throwException");
      }
   }

   public static void doesNotThrowException()
   {
      try
      {
         System.out.println("doesNotThrowException method.");
      }
      catch (Exception exception)
      {
         System.err.println(exception);
      }
      finally
      {
         System.err.println("Finally executed in doesNotThrowException");
      }

      System.out.println("End of doesNotThrowException method.");
   }

}
