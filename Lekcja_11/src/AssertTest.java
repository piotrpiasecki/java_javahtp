import java.util.Arrays;
import java.util.Scanner;

public class AssertTest
{
   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);

      System.out.print("Type an integer number from 0 to 10: ");
      int number = input.nextInt();

      assert ( number >= 0 && number <= 10) : "Incorrect number: " + number;

      System.out.printf("You've typed number: %d", number);
   }
}
