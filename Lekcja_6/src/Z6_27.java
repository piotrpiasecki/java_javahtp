import java.util.Scanner;
// import org.apache.commons.lang3.StringUtils;

public class Z6_27
{
   public static void NWD()
   {
      Scanner input = new Scanner(System.in);
      System.out.print("Podaj parę liczb, oddzielonych spacją, dla których szukasz NWD: ");
      String numbers = input.nextLine();

      while (! numbers.equals("koniec"))
      {
         String naturalNumbers = areNaturalNumbers(numbers);

         int a = Integer.valueOf(naturalNumbers.split("\\s+")[0]);
         int b = Integer.valueOf(naturalNumbers.split("\\s+")[1]);
         int c = 0;

         while (b != 0)
         {
            c = a % b;
            a = b;
            b = c;
         }

         System.out.println("NWD tych liczb to: " + a);

         System.out.print("Podaj parę liczb, oddzielonych spacją, dla których szukasz NWD: ");
         numbers = input.nextLine();
      }
      System.out.print("Koniec programu");
   }

   public static String areNaturalNumbers(String numbers)
   {
      String[] aAndB = numbers.split("\\s+");

      while ( ( aAndB.length != 2 ) || ( ! ( isNatural(aAndB[0]) && isNatural(aAndB[1]) ) ) )
      {
         System.out.print("Musisz podać dwie liczby naturalne! Wprowadź je jeszcze raz: ");
         Scanner input = new Scanner(System.in);
         aAndB = input.nextLine().split("\\s+");
      }

      numbers = aAndB[0] + " " + aAndB[1];
      return numbers;
   }

   public static boolean isNatural(String input)
   {
      if (input.matches("[+]?(0|[1-9]\\d*)"))
      {
         return true;
      }
      else
      {
         return false;
      }
   }
}
