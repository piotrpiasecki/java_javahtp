import java.util.Scanner;

public class Z6_24
{
   public static void isPerfect()
   {
      Scanner input = new Scanner(System.in);
      System.out.print("Podaj liczbę do sprawdzenia: ");

      String int1 = input.next();
      Z6_21.isNaturalNumber(int1);
      int sumOfDivisors = 0;
      for (int i = 1; i < Integer.valueOf(int1); i++)
      {
         if ((Integer.valueOf(int1) % i) == 0) { sumOfDivisors += i; }
      }

      System.out.printf("Liczba %s %s liczbą doskonałą.", int1,
         (Integer.valueOf(int1) == sumOfDivisors) ? "jest" : "nie jest");
   }
}
