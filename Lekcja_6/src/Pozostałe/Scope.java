package Pozostałe;

public class Scope
{
   private static int x = 1;

   public static void main(String[] args)
   {
      int x = 5;

      System.out.printf("Lokalne x w main wynosi %d%n", x);

      useLocalVariable();
      useField();
      useLocalVariable();
      useField();

      System.out.printf("%nLokalne x w main wynosi %d%n", x);
   }

   public static void useLocalVariable()
   {
      int x = 25;
      System.out.printf("%nLokalne x przy wejściu do metody useLocalVariable wynosi %d%n", x);
      ++x;
      System.out.printf("Lokalne x przed wyjściem z metody useLocalVariable wynosi %d%n", x);
   }


    public static void useField()
    {
       System.out.printf("%nPole x przy wejściu do metody useField wynosi %d%n", x);
       x *= 10;
       System.out.printf("Pole x przed wyjściem z metody useField wynosi %d%n", x);
    }
 }