import java.util.Scanner;

public class Z6_16
{

   String a;
   String b;
   Scanner input = new Scanner(System.in);

   public void start()
   {
      System.out.print("Podaj liczby oddzielając je spacją. Gdy zechcesz zakończyć wpisz \"koniec\": ");
      setAB();
   }

   private void setAB()
   {
      this.a = input.next();
      if (!(isKoniec(this.a)))
      {
         this.b = input.next();
      }
      else
      {
         System.out.print("Koniec programu");
      }
   }

   public static void isMultipleFinal(Z6_16 number)
   {
      while (!(isKoniec(number.a)))
      {
         correctInput(number);
         if (!(isKoniec(number.a)))
         {
            System.out.printf("%s %s wielokrotnością %s %n", number.b,
               ((Integer.valueOf(number.b) % Integer.valueOf(number.a)) == 0) ? "jest" : "nie jest", number.a);
            number.start();
         }
      }
   }

   private static void correctInput(Z6_16 number)
   {
      Scanner input = new Scanner(System.in);
      while (!((isNumber(number.a) || isKoniec(number.a)) && isNumber(number.b)))
      {
         if (isKoniec(number.a))
         {
            break;
         }
         System.out.print("Jedno z pól jest nieprawidłowe! " +
         "Wprowadź parę liczb oddzieloną spacją lub \"koniec\" na nowo: ");
         number.setAB();
      }
   }

   private static boolean isNumber(String input)
   {
      return input.matches("-?(0|[1-9]\\d*)");
   }

   private static boolean isKoniec(String input)
   {
      return input.equals("koniec");
   }

   public Z6_16()
   {
      start();
   }

   private String getA(Z6_16 number)
   {
      return number.a;
   }

   private String getB(Z6_16 number)
   {
      return number.b;
   }
}
