import java.util.Scanner;

public class Z6_21
{
   public static void displayDigits()
   {
      System.out.print("Podaj liczbę naturalną: ");
      Scanner input = new Scanner(System.in);
      String int1 = input.next();

      isNaturalNumber(int1);

      for (int i = 1; i <= (int1.length() - 1); i++)
      {
         System.out.print(int1.charAt(i-1) + "  ");
      }
      System.out.print(int1.charAt(int1.length() - 1));
   }

   public static String isNaturalNumber(String intInput)
   {
      while (!(intInput.matches("[+]?(0|[1-9]\\d*)")))
      {
         Scanner input = new Scanner(System.in);
         System.out.print("Podana wartość nie jest liczbą naturalną. Podaj inną wartość: ");
         intInput = input.next();
      }
      return intInput;
   }
}
