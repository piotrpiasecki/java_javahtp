import java.util.Scanner;

public class Z6_18
{
   private char fillCharacter;
   private int side;

   public void setFillCharacter()
   {
      System.out.print("Podaj znak wypełnienia: ");
      Scanner input = new Scanner(System.in);
      char character = input.next().charAt(0);
      this.fillCharacter = character;
   }

   public void setSide()
   {
      System.out.print("Podaj długość boku: ");
      Scanner input = new Scanner(System.in);
      String sideLength = input.next();

      while (!(sideLength.matches("[+]?(0|[1-9]\\d*)")))
      {
         System.out.print("Podaj liczbę naturalną! ");
         sideLength = input.next();
      }
      this.side = Integer.valueOf(sideLength);
   }

   public void squareOfAsterisks()
   {
      this.side = side;
      this.fillCharacter = fillCharacter;
      for (int i = 1; i <= side; i++)
      {
         for (int j = 1; j <= side; j++)
         {
            System.out.print(fillCharacter);
         }
         System.out.println();
      }
   }
}
