package Craps;

import java.security.SecureRandom;
import java.util.Scanner;

public class Craps
{
   private static final SecureRandom randomNumbers = new SecureRandom();

   private enum Status {CONTINUE, WON, LOST}

   private static final int SNAKE_EYES = 2;
   private static final int TREY = 3;
   private static final int SEVEN = 7;
   private static final int YO_LEVEN = 11;
   private static final int BOX_CARS = 12;

   public static void main(String[] args)
   {
      int myPoint = 0;

      Status gameStatus;

      waitForEnter();

      int sumOfDice = rollDice();

      switch (sumOfDice)
      {
         case SEVEN:
         case YO_LEVEN:
            gameStatus = Status.WON;
            break;
         case SNAKE_EYES:
         case TREY:
         case BOX_CARS:
            gameStatus = Status.LOST;
            break;
         default:
            gameStatus = Status.CONTINUE;
            myPoint = sumOfDice;
            System.out.printf("Punkt to %d%n", myPoint);
            break;
      }

      while (gameStatus == Status.CONTINUE)
      {
         waitForEnter();
         sumOfDice = rollDice();

         if (sumOfDice == myPoint)
         {
            gameStatus = Status.WON;
         }
         else
         {
            if (sumOfDice == SEVEN)
            {
               gameStatus = Status.LOST;
            }
//            else
//            {
//               gameStatus = Status.CONTINUE;
//            }
         }
      }

      if (gameStatus == Status.WON)
      {
         System.out.print("Wygrałeś!");
      }
      else
      {
         System.out.print("Przegrałeś!");
      }
   }

   private static int rollDice()
   {
      int die1 = 1 + randomNumbers.nextInt(6);
      int die2 = 1 + randomNumbers.nextInt(6);

      int sum = die1 + die2;

      System.out.printf("Wyrzuciłeś %d + %d = %d%n", die1, die2, sum);

      return sum;
   }

   private static void waitForEnter()
   {
      Scanner input = new Scanner(System.in);

      System.out.printf("Wciśnij \"Enter\", aby rzucić kośćmi %n");
      input.nextLine();
   }
}
